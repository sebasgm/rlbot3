
import cv2
import cv2.cv as cv
import numpy as np
from cv import *

face_cascade = cv2.CascadeClassifier('../haarcascades/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('../haarcascades/haarcascade_eye.xml')

counter = 0

def detect_faces(capture):
    global counter

    frame = QueryFrame(capture)
    img = np.asarray(frame[:,:])
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#    cv2.imshow('img',img)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
#            print "got it! ", counter, "times"
#            counter = counter + 1
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
            cv2.imshow('img',img)


def main():
    capture = CaptureFromCAM(1)

    while True:

        detect_faces(capture)
  #      frame = QueryFrame(capture)
  #      img = np.asarray(frame[:,:])
  #      gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


        #faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        #for (x,y,w,h) in faces:
        #    cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        #    roi_gray = gray[y:y+h, x:x+w]
        #    roi_color = img[y:y+h, x:x+w]
        #    eyes = eye_cascade.detectMultiScale(roi_gray)
        #    for (ex,ey,ew,eh) in eyes:
        #        print "got it! ", counter, "times"
        #        counter = counter + 1
#                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
#                cv2.imshow('img',img)

        c = WaitKey(20)

        if(c!=-1):
            break


    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return


if __name__ == "__main__":
    main()
