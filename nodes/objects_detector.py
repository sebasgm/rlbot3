#!/usr/bin/env python

PKG = 'rlbot3'

import sys 
import rospy
from time import strftime, gmtime
import time 

import cv2
import cv2.cv as cv
from cv import *
import numpy as np
from math import atan2, pi

from std_msgs.msg import *
from geometry_msgs.msg import Twist


rospy.init_node('objects_detector', anonymous=True) # Node initialization

# Publishers and mgs declarations
pub_objects_state = rospy.Publisher('/rlbot3/objects_node_status', Bool) # This topic is for publishing a sort of "keep alive" msg
pub_new_vel = rospy.Publisher('/rlbot3/cmd_vel', Twist) # This topic updates velocities based on camera processing 

new_vel = Twist()


# Parameter initialization
if not rospy.has_param('light_factor'):
    rospy.set_param('light_factor', 1.5)
    light_factor = rospy.get_param('light_factor')
else:
    light_factor = rospy.get_param('light_factor')

init_time = int(strftime("%S", gmtime())) # Set time for keep_alive msg 
angular_update = False


def process_and_send(posX, area, threshold):
    # NOTE: There is still not encoders, so it function assumes thatthe robot will turn for a fixed time
    
    global light_factor
    global angular_update

    light_factor = rospy.get_param('light_factor')
    updated_threshold = light_factor*threshold

    rospy.loginfo("Threshold: %.1f", updated_threshold)
    if area >= updated_threshold:
        if angular_update is False:
            angular_update = True
            if area <= updated_threshold*2.5: # Moves forwards for a lower threshold
                rospy.loginfo("Far Area: %.1f", area)
                new_vel.linear.x = 0.76
            else:
                if area >= updated_threshold*4: # Moves backwards for a higher threshold
                    rospy.loginfo("Near Area: %.1f", area)
                    new_vel.linear.x = (-0.76)
        else:
            angular_update = False
            if posX <= 70.0: 
                new_vel.angular.z = (-8.2)
            else:
                if posX >= 240.0: 
                    new_vel.angular.z = 8.2

        pub_new_vel.publish(Twist(new_vel.linear, new_vel.angular)) 
    
        # Delay before stopping movement
        time.sleep(0.2) # This values acts along with the set velocities for a better response
        

def objects_detector_node():
    global init_time

    object_data = cameraProcessing()

    rospy.loginfo("'OBJECTS DETECTOR node' ACTIVE")
    rospy.loginfo("Velocity msgs are published at '/rlbot3/cmd_vel'")
    rospy.sleep(1)

    while not rospy.is_shutdown():

        object_data.process_frame()
        process_and_send(object_data.posX, object_data.area, object_data.area_threshold)

        current_time = int(strftime("%S", gmtime()))
        diff_time = abs(current_time - init_time)
        if diff_time >= 2:
            init_time = int(strftime("%S", gmtime()))
            pub_objects_state.publish(Bool(True))

        # Stops the robot and cleans object_data
        object_data.area = 1.0
        new_vel.linear.x = 0.0
        new_vel.angular.z = 0.0
        pub_new_vel.publish(Twist(new_vel.linear, new_vel.angular))

    rospy.spin()


class cameraProcessing(object):

    def __init__(self):

        self.capture = None
        self.frame = None
        self.posX = 0
        self.posY = 0
        self.prevX = 0
        self.prevY = 0
        self.area = 0
        self.area_threshold = 150000.0
        #self.processed_object = [1, 1]
        #self.displacement = 0
        self.imgFiltered = None

        #self.thresh_window = "threshold"
        #NamedWindow(self.thresh_window, 1)

        self.capture = CaptureFromCAM(0) 
        SetCaptureProperty(self.capture, CV_CAP_PROP_FRAME_WIDTH, 320)
        SetCaptureProperty(self.capture, CV_CAP_PROP_FRAME_HEIGHT, 240)


    def get_thresholded_image(self):
        """
        Returns thresholded image from raw capture
        based on the colors inside the defined range
        """

        #Creates img from webcam frame
        img = CreateImage(GetSize(self.frame), 8, 3)

        # Converts a BGR image to HSV
        CvtColor(self.frame, img, CV_BGR2HSV)

        # Creates a new frame for placing filtered img
        imgFiltered = CreateImage(GetSize(img), 8, 1)

        #Takes source, lowerbound color, upperbound color and destination
        #Converts the pixel values lying within the range to 255 and store these in
        #destination img created
        InRangeS(img, (24, 55, 55), (65, 190, 190), imgFiltered) # Detects GREEN color

        return imgFiltered

    def process_frame(self):
        """
        Takes the img from current frame and returns position
        of detected object
        """
        light_factor = rospy.get_param('light_factor')

        # Creates img from frame and aplies a low pass filter
        self.frame = QueryFrame(self.capture)

        # Generates a thresholded img from a certain color channel
        self.imgFiltered = self.get_thresholded_image()

        # Calculates the moments
        mat = GetMat(self.imgFiltered)
        moments = Moments(mat, 0)
        self.area = GetCentralMoment(moments, 0, 0)
        moment10 = GetSpatialMoment(moments, 1, 0)
        moment01 = GetSpatialMoment(moments, 0, 1)

        # prevX and prevY store the previous positions
        self.prevX = self.posX
        self.prevY = self.posY

        # print ' Current main Area: ' + str(self.area)
        # ShowImage(self.thresh_window, imgFiltered)

        #rospy.loginfo(self.area)
        if(self.area >= 100000.0):
            #Calculates the coordinate position of the centroid
            self.posX = int(moment10 / self.area)
            #self.posY = int(moment01 / self.area)
            #self.displacement = abs(self.posX) - abs(self.prevX)
            #self.processed_object[0] = self.posX
            #self.processed_object[1] = self.area
            #self.processed_object[1] = (self.area - self.prevArea)
            #self.prevArea = self.area

            #ShowImage(self.thresh_window, self.imgFiltered)


if __name__ == '__main__':

    try:
        objects_detector_node()
    except rospy.ROSInterruptException:
        GPIO.cleanup()
