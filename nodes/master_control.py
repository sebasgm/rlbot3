#!/usr/bin/env python

PKG = 'rlbot3'

import sys
import rospy
import time
from time import strftime, gmtime

from std_msgs.msg import *

import Adafruit_BBIO.GPIO as GPIO


rospy.init_node('master_control', anonymous=True) # Node initialization

init_time_cliff = int(strftime("%S", gmtime()))
init_time_objects = int(strftime("%S", gmtime()))
init_time_face = int(strftime("%S", gmtime()))
init_time_vel = int(strftime("%S", gmtime()))

# Publishers definition
pub_status = rospy.Publisher('/rlbot3/global_status', String)


def cliff_status_callback(callback_data):
    global init_time_cliff
    init_time_cliff = int(strftime("%S", gmtime()))

def vel_status_callback(callback_data):
    global init_time_vel
    init_time_vel = int(strftime("%S", gmtime()))

def objects_status_callback(callback_data):
    global init_time_objects
    init_time_objects = int(strftime("%S", gmtime()))

def face_status_callback(callback_data):
    global init_time_face
    init_time_face = int(strftime("%S", gmtime()))


def master_control_node():
    """
    This node is capable of publishing alerts via one specific ROS Topic
    """

    global init_time_cliff
    global init_time_objects
    global init_time_face
    global init_time_vel

    control_msg = " "

    rospy.loginfo("'MASTER Control node' ACTIVE")
    rospy.loginfo("Status information is being published at '/rlbot3/global_status'")

    rospy.sleep(1)

    while not rospy.is_shutdown():

        rospy.Subscriber('/rlbot3/objects_node_status', Bool, objects_status_callback)
        rospy.Subscriber('/rlbot3/face_node_status', Bool, face_status_callback)
        rospy.Subscriber('/rlbot3/cliff_sensor_node_status', Bool, cliff_status_callback)
        rospy.Subscriber('/rlbot3/move_control_node_status', Bool, vel_status_callback)

        current_time = int(strftime("%S", gmtime()))
        diff_time_cliff = abs(current_time - init_time_cliff)
        diff_time_vel = abs(current_time - init_time_vel)
        diff_time_objects = abs(current_time - init_time_objects)
        diff_time_face = abs(current_time - init_time_face)

        if diff_time_cliff >= 6:
            control_msg = "CLIFF NODE = DOWN"
            #rospy.logwarn("'Cliff sensor node' is NOT responding")
        else:
            control_msg = "CLIFF NODE = OK"
        if diff_time_vel >= 6:
            control_msg = control_msg + "|MOVE NODE = DOWN"
            #rospy.logwarn("'Move control node' is NOT responding")
        else:
            control_msg = control_msg + "|MOVE NODE = OK"
        if diff_time_objects >= 6:
            control_msg = control_msg + "|OBJECTS NODE = DOWN"
            #rospy.logwarn("'Camera node' is NOT responding")
        else:
            control_msg = control_msg + "|OBJECTS NODE = OK"
        if diff_time_face >= 6:
            control_msg = control_msg + "|FACE NODE = DOWN"
            #rospy.logwarn("'Camera node' is NOT responding")
        else:
            control_msg = control_msg + "|FACE NODE = OK"

        pub_status.publish(String(control_msg))

    time.sleep(3)
    rospy.spin()


if __name__ == '__main__':
    try:
        master_control_node()
    except rospy.ROSInterruptException: pass



