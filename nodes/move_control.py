#!/usr/bin/env python

PKG = 'rlbot3'

import sys 
import rospy
from time import strftime, gmtime
import time

import numpy as np
from math import atan2, pi

from std_msgs.msg import *
from geometry_msgs.msg import Twist

import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.GPIO as GPIO

rospy.init_node('move_control', anonymous=True) # Node initialization

## Example command line Twist msgs execution:
#rostopic pub -r 1 /rlbot3/cmd_vel geometry_msgs/Twist  '{linear:  {x: 0.8, y: 0.0, z: 0.0}, angular: {x: 0.0,y: 0.0,z: 0.0}}'

# Topic and msg type
pub_status = rospy.Publisher('/rlbot3/move_control_node_status', Bool)

# Global variables definitions
init_time = int(strftime("%S", gmtime()))
diff_time = init_time
twist_time = init_time
rlbot_base_diameter = 0.20 #In meters
rlbot_wheel_radius = 0.04  #In meters

# Parameter initialization
if not rospy.has_param('camera_pos'):
    rospy.set_param('camera_pos', '/left')
    camera_pos = rospy.get_param('camera_pos')
else:
    camera_pos = rospy.get_param('camera_pos')


def start_PWM():
    # H bridge EN left wheel
    PWM.start("P8_13", 0.0)

    # H bridge EN right wheel
    PWM.start("P9_14", 0.0)

def stop_motors():

    # Left wheels H bridge IN's
    GPIO.output("P8_14", GPIO.LOW)
    GPIO.output("P8_17", GPIO.LOW)
    # Right wheels H bridge IN's
    GPIO.output("P9_23", GPIO.LOW)
    GPIO.output("P9_15", GPIO.LOW)

    # H bridge EN left wheel
    PWM.set_duty_cycle("P8_13", 0.0)
    # H bridge EN right wheel
    PWM.set_duty_cycle("P9_14", 0.0)

# Makes calculation for getting independent values for each wheels, from a linear and angular value received from outside
def get_wheels_vel(linear_vel_x, ang_vel_z):

    if linear_vel_x < 0:
        ang_vel_z = -ang_vel_z

    aux_right_wheel_vel = 0.5*(((2*linear_vel_x)/rlbot_wheel_radius) - ((ang_vel_z*rlbot_base_diameter)/rlbot_wheel_radius))
    aux_left_wheel_vel = 0.5*(((2*linear_vel_x)/rlbot_wheel_radius) + ((ang_vel_z*rlbot_base_diameter)/rlbot_wheel_radius))
    left_wheel_vel = 2.5*aux_left_wheel_vel
    right_wheel_vel = 2.5*aux_right_wheel_vel

    return left_wheel_vel, right_wheel_vel

# Sets corresponding PWM percentage for each wheel, and defines the direction
def set_wheels_pwm(left_wheel_vel, right_wheel_vel):

    # Sets inputs and PWM to zero to avoid undesirables movements while configuring
    stop_motors()

    # If velocities have opposite direction then it must be set correct direction
    if (left_wheel_vel*right_wheel_vel) < 0.0:
        if left_wheel_vel < 0.0:
            # Left wheels H bridge IN's
            GPIO.output("P8_14", GPIO.LOW)
            GPIO.output("P8_17", GPIO.HIGH)
            # Right wheels H bridge IN's
            GPIO.output("P9_15", GPIO.HIGH)
            GPIO.output("P9_23", GPIO.LOW)
        else:
            # Left wheels H bridge IN's
            GPIO.output("P8_14", GPIO.HIGH)
            GPIO.output("P8_17", GPIO.LOW)
            # Right wheels H bridge IN's
            GPIO.output("P9_15", GPIO.LOW)
            GPIO.output("P9_23", GPIO.HIGH)
    elif left_wheel_vel < 0.0 and right_wheel_vel < 0.0:
        # Left wheels H bridge IN's
        GPIO.output("P8_14", GPIO.LOW)
        GPIO.output("P8_17", GPIO.HIGH)
        # Right wheels H bridge IN's
        GPIO.output("P9_15", GPIO.LOW)
        GPIO.output("P9_23", GPIO.HIGH)
    else:
        # Left wheels H bridge IN's
        GPIO.output("P8_14", GPIO.HIGH)
        GPIO.output("P8_17", GPIO.LOW)
        # Right wheels H bridge IN's
        GPIO.output("P9_15", GPIO.HIGH)
        GPIO.output("P9_23", GPIO.LOW)
        right_wheel_vel = right_wheel_vel*1.37 # Moving forwards this wheel is slightly slower

    # Values for PWM need to be between 0 and 100 and be positive
    left_wheel_vel = abs(left_wheel_vel)
    right_wheel_vel = abs(right_wheel_vel)
    if left_wheel_vel > 55.0:
        left_wheel_vel = 55.0

    if right_wheel_vel > 55.0:
        right_wheel_vel = 55.0
    
    # H bridge EN left wheel
    PWM.set_duty_cycle("P8_13", left_wheel_vel)
    # H bridge EN right wheel
    PWM.set_duty_cycle("P9_14", right_wheel_vel)

def face_action_callback(face_data):
    global camera_pos
    
    camera_pos = rospy.get_param('camera_pos')

    if face_data.data is True:
        if camera_pos == '/left':
            # Rotate in place for to a fixed vel, por fixed time
            left_wheel_vel, right_wheel_vel = get_wheels_vel(0.0, -20.0)
            set_wheels_pwm(left_wheel_vel, right_wheel_vel)
            time.sleep(0.62)
            set_wheels_pwm(0.0, 0.0)
        elif camera_pos == '/right':
            # Rotate in place for to a fixed vel, por fixed time
            left_wheel_vel, right_wheel_vel = get_wheels_vel(0.0, 20.0)
            set_wheels_pwm(left_wheel_vel, right_wheel_vel)
            time.sleep(0.37)
            set_wheels_pwm(0.0, 0.0)


def stop_control_callback(control_data):

    if control_data > 20.0:
        rospy.loginfo("Cliff alert! stopping motors")
        stop_motors()
        PWM.stop("P9_14")
        PWM.stop("P8_13")

def objects_action_callback(move_data):

    global twist_time
    twist_time = int(strftime("%S", gmtime()))

    linear_vel_x = move_data.linear.x
    ang_vel_z = move_data.angular.z

    left_wheel_vel, right_wheel_vel = get_wheels_vel(linear_vel_x, ang_vel_z)
    set_wheels_pwm(left_wheel_vel, right_wheel_vel)


def move_control_node():
    global init_time
    global diff_time
    global twist_time
    global rlbot_base_diameter
    global rlbot_wheel_radius

    # Left wheels H bridge IN's
    GPIO.setup("P8_14", GPIO.OUT)
    GPIO.setup("P8_17", GPIO.OUT)
    # Right wheels H bridge IN's
    GPIO.setup("P9_15", GPIO.OUT)
    GPIO.setup("P9_23", GPIO.OUT)

    # Configure PWM outputs
    start_PWM()
    
    time.sleep(1)

    # Disables H bridge Inputs and PWM 
    stop_motors()

    # Command line logging data
    rospy.loginfo("Move node setup finished")

    rospy.Subscriber('/rlbot3/cliff_sensor_measurement', Float32, stop_control_callback)
    rospy.Subscriber('/rlbot3/cmd_vel', Twist, objects_action_callback)
    rospy.Subscriber('/rlbot3/face_detected', Bool, face_action_callback)

    rospy.loginfo("'MOVE Control node' ACTIVE")
    rospy.sleep(1)

    while not rospy.is_shutdown():

        current_time = int(strftime("%S", gmtime()))
        diff_time = abs(current_time - init_time)

        # MSG generation for global status report
        if diff_time >= 3.5:
            init_time = int(strftime("%S", gmtime()))
            pub_status.publish(Bool(True))

        diff_time = abs(current_time - twist_time)

        # MSG generation for global status report
        if diff_time >= 5:
            twist_time = int(strftime("%S", gmtime()))
            # Stop motors to avoid undiserable movements
            stop_motors()

            # Debugging
            #rospy.loginfo("Movement stoped")

    rospy.spin()


if __name__ == '__main__':
    try:
        move_control_node()
    except rospy.ROSInterruptException:
        stop_motors()
        PWM.cleanup()
