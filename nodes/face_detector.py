#!/usr/bin/env python

PKG = 'rlbot3'

import sys 
import rospy
from time import strftime, gmtime
import time 

import cv2
import cv2.cv as cv
from cv import *
import numpy as np
from math import atan2, pi

from std_msgs.msg import *

import Adafruit_BBIO.GPIO as GPIO; print GPIO


init_time = int(strftime("%S", gmtime())) # Set time for keep alive msg 
init_processing_time = int(strftime("%S", gmtime())) # Set time for keep alive msg 
camera_pos = None

rospy.init_node('face_detector', anonymous=True) # Node initialization

# Topic and msg type, names
pub_face_state = rospy.Publisher('/rlbot3/face_node_status', Bool) # This topic is for publishing a sort of "keep alive" msg
pub_face_data = rospy.Publisher('/rlbot3/face_detected', Bool) # This topic exposes the current output from camera processing node

# Parameter initialization
if not rospy.has_param('camera_pos'):
    rospy.set_param('camera_pos', '/left')

def process_and_send(rect_width):
    global init_processing_time

    # After 3 seconds since last valid detected face, the state is able to be updated 
    current_time = int(strftime("%S", gmtime()))
    diff_time = abs(current_time - init_processing_time)

    if (rect_width > 400.0) and (diff_time >= 2):
        init_processing_time = int(strftime("%S", gmtime()))
        pub_face_data.publish(Bool(True))
    else:
        pub_face_data.publish(Bool(False))

    #rospy.loginfo("Rectangle width:%.1f", rect_width)

def face_detector_node():
    global init_time

    camera_data = cameraProcessing()

    rospy.loginfo("'FACE DETECTOR node' ACTIVE")
    rospy.loginfo("Detected state is being published at '/rlbot3/face_detected'")
    rospy.sleep(1)

    while not rospy.is_shutdown():

        camera_data.detect_faces()
        process_and_send(camera_data.rect_width)

        current_time = int(strftime("%S", gmtime()))
        diff_time = abs(current_time - init_time)
        if diff_time >= 2:
            init_time = int(strftime("%S", gmtime()))
            pub_face_state.publish(Bool(True))

        camera_data.rect_width = 0.0
#        time.sleep(0.25)

    rospy.spin()


class cameraProcessing(object):

    def __init__(self):

        self.face_cascade = cv2.CascadeClassifier('../haarcascades/haarcascade_frontalface_default.xml')
        self.eye_cascade = cv2.CascadeClassifier('../haarcascades/haarcascade_eye.xml')

        self.frame = None
        self.capture = None
        self.rect_width = 0
        self.capture = CaptureFromCAM(0)

    def detect_faces(self):

        self.frame = QueryFrame(self.capture)
        img = np.asarray(self.frame[:, :])
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), (25, 0, 0), 2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            eyes = self.eye_cascade.detectMultiScale(roi_gray)
            for (ex, ey, ew, eh) in eyes:
                self.rect_width = int(x + w)

#                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
#                cv2.imshow('img',img)


if __name__ == '__main__':

    try:
        face_detector_node()
    except rospy.ROSInterruptException:
        GPIO.cleanup()
