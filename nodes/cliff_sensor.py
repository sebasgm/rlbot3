#!/usr/bin/env python

PKG = 'rlbot3'

import sys
import rospy
from time import strftime, gmtime
import time

import numpy as np
from math import atan2, pi

from std_msgs.msg import *
import std_srvs.srv

import Adafruit_BBIO.GPIO as GPIO; print GPIO
import Adafruit_BBIO.ADC as ADC


ADC.setup()

rospy.init_node('cliff_sensor', anonymous=True) # Node initialization

# Publishers definition
pub_status = rospy.Publisher('/rlbot3/cliff_sensor_node_status', Bool) # Publishes a sort of "keep alive" msg
pub_measurement = rospy.Publisher('/rlbot3/cliff_sensor_measurement', Float32) # Exposes the actual value of the IR distance measure

init_time = int(strftime("%S", gmtime())) # Set time for keep alive msg 

def action_callback(distance_data):

    # To avoid zero division error
    if distance_data == 0:
        distance = 0.0
    else:
        # Process the data in order to return a real distance value
        distance=(30.0+((20.0*((1.0/(float(distance_data)*1.8))-1.4068))/0.71562)) # TODO: Check if gathered values are consistent
        if distance < 5.0 or distance > 80.0:
            pass
        else:
            pub_measurement.publish(Float32(distance))
            #Debugging
            rospy.loginfo("raw distance: %.2f", distance_data)


def cliff_sensor_node():
    global init_time

    while not rospy.is_shutdown():

        distance_read = ADC.read_raw("AIN0")
        rospy.sleep(0.25) # Add a delay to wait for a well formed convertion
        rospy.loginfo("raw distance: %.2f", distance_read)

        reset_time = int(strftime("%S", gmtime()))
        diff_time = abs(reset_time - init_time)

        action_callback(distance_read)

        if diff_time >= 2:
            init_time = int(strftime("%S", gmtime()))
            pub_status.publish(Bool(True))

    rospy.sleep(0.25)

    rospy.spin()


if __name__ == '__main__':
    try:
        cliff_sensor_node()
    except rospy.ROSInterruptException:
        GPIO.cleanup()
