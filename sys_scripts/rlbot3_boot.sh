#!/bin/bash

# This script is used to launch all RLBOT3 components
# needed to be fully functional
# README: In order to work it should be located at /etc/init.d

sessions=`tmux list-session | wc -l` > /dev/null 2>&1

echo "*** RLBOT3 system booting ***"

sleep 5

if [ ${sessions} -gt 0 ]; then
  echo "There is an existent tmux session running"
else
  echo "Starting RLBOT3 components"
  # Starts server without any session on it
  tmux start-server
  # Starts a new session called 'rlbot3' with first 
  # windows called 'rosmaster'. -d is for accessing 
  # from any terminal 
  tmux new-session -d -s rlbot3 -n rosmaster
  # Executes line between ' ' and adds 'Enter' (C-m)
  tmux send-keys -t rlbot3:0 "source rlbot3_setup.bash" C-m
  tmux send-keys -t rlbot3:0 "roscore" C-m

  sleep 3

  # Renames second window. -t groups the window in the current 
  # target-session
  tmux new-window -t rlbot3:1 -n move-control-node
  tmux send-keys -t rlbot3:1 "source rlbot3_setup.bash" C-m
  tmux send-keys -t rlbot3:1 "cd /home/ubuntu/ros_ws/src/rlbot3/nodes" C-m
  tmux send-keys -t rlbot3:1 "rosrun rlbot3 move_control.py" C-m

  tmux new-window -t rlbot3:2 -n object-detector-node
  tmux send-keys -t rlbot3:2 "source rlbot3_setup.bash" C-m
  tmux send-keys -t rlbot3:2 "cd /home/ubuntu/ros_ws/src/rlbot3/nodes" C-m
  tmux send-keys -t rlbot3:2 "rosrun rlbot3 objects_detector.py" C-m

  #tmux new-window -t rlbot3:3 -n master-control-node
  #tmux send-keys -t rlbot3:3 "source rlbot3_setup.bash" C-m
  #tmux send-keys -t rlbot3:3 "cd /home/ubuntu/ros_ws/src/rlbot3/nodes" C-m
  #tmux send-keys -t rlbot3:3 "rosrun rlbot3 master_control.py" C-m
  
  #tmux new-window -t rlbot3:4 -n cliff-control-node
  #tmux send-keys -t rlbot3:4 "source rlbot3_setup.bash" C-m
  #tmux send-keys -t rlbot3:4 "cd /home/ubuntu/ros_ws/src/rlbot3" C-m
  #tmux send-keys -t rlbot3:4 "rosrun rlbot3 cliff_sensor.py" C-m
fi
